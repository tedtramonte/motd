#!/bin/bash

#### CONFIG ####
DISK_MAX_USAGE=90
DISK_BAR_USAGE=50
DOCKER_COLUMNS=2

# Colors
DIM="\e[2m"
TITLE="\e[4;39m\e[1;39m"
WHITE="\e[1;39m"
GREEN="\e[1;32m"
RED="\e[1;31m"
NC="\e[0m"

#### PRETTY HOSTNAME ####
if command -v figlet &> /dev/null
then
  echo -e "$GREEN"
  /usr/bin/env figlet "$(hostname)"
  echo -e "$NC"
fi

#### SYSTEM INFO ####
# Get load averages
IFS=" " read -r LOAD1 LOAD5 LOAD15 <<< "$(cat /proc/loadavg | awk '{ print $1,$2,$3 }')"
# Get free memory
IFS=" " read -r USED FREE TOTAL <<< "$(free -htm | grep "Mem" | awk '{ print $3,$4,$2 }')"
# Get processes
PROCESS=$(ps -eo user=|sort|uniq -c | awk '{ print $2 " " $1 }')
PROCESS_ALL=$(echo "$PROCESS"| awk '{print $2}' | awk '{ SUM += $1} END { print SUM }')
PROCESS_ROOT=$(echo "$PROCESS"| grep root | awk '{print $2}')
PROCESS_USER=$(echo "$PROCESS"| grep -v root | awk '{print $2}' | awk '{ SUM += $1} END { print SUM }')
# Get processors
PROCESSOR_NAME=$(grep "model name" /proc/cpuinfo | cut -d ' ' -f3- | awk '{print $0}' | head -1)
PROCESSOR_COUNT=$(grep -ioP 'processor\t:' /proc/cpuinfo | wc -l)

echo -e "
${TITLE}System Info:$NC
${WHITE}  Hostname....:$NC $(echo -e "$(hostname)")
${WHITE}  Distro......:$NC $(cat /etc/*release | grep "PRETTY_NAME" | cut -d "=" -f 2- | sed 's/"//g')
${WHITE}  Kernel......:$NC $(uname -sr)

${WHITE}  Uptime......:$NC $(uptime -p)
${WHITE}  Load........:$NC $GREEN$LOAD1$NC (1m), $GREEN$LOAD5$NC (5m), $GREEN$LOAD15$NC (15m)
${WHITE}  Processes...:$NC $GREEN$PROCESS_ROOT$NC (root), $GREEN$PROCESS_USER$NC (user), $GREEN$PROCESS_ALL$NC (total)

${WHITE}  CPU.........:$NC $PROCESSOR_NAME ($GREEN$PROCESSOR_COUNT$NC vCPU)
${WHITE}  Memory......:$NC $GREEN$USED$NC used, $GREEN$FREE$NC free, $GREEN$TOTAL$NC total"


#### DISK SPACE ####
mapfile -t dfs < <(df -H -x zfs -x squashfs -x tmpfs -x devtmpfs --output=target,pcent,size | tail -n+2)
echo -e "
${TITLE}Disk Usage:$NC"

for line in "${dfs[@]}"
do
  if ! echo "$line" | grep "/run/k3s/containerd/" &> /dev/null
  then
    # Get disk usage
    usage=$(echo "$line" | awk '{print $2}' | sed 's/%//')
    used_width=$((($usage*$DISK_BAR_USAGE)/100))
    # Color is GREEN if usage < DISK_MAX_USAGE, else RED
    if [ "${usage}" -ge "${DISK_MAX_USAGE}" ]
    then
      color=$RED
    else
      color=$GREEN
    fi
    # Print GREEN/RED bar until used_width
    bar="[${color}"
    for ((i=0; i<$used_width; i++))
    do
      bar+="="
    done
    bar+="$NC"
    # Print DIM bar until end
    bar+="${WHITE}${DIM}"
    for ((i=$used_width; i<$DISK_BAR_USAGE; i++))
    do
      bar+="="
    done
    bar+="${NC}]"
    # Print usage line & bar
    echo "${line}" | awk '{ printf("%-31s%+3s used out of %+4s\n", $1, $2, $3); }' | sed -e 's/^/  /'
    echo -e "${bar}" | sed -e 's/^/  /'
  fi
done


#### DOCKER STATUS ####
mapfile -t containers < <(docker ps -a --format '{{.Names}}\t{{.Status}}' | awk '{ print $1,$2 }')

out=""
for i in "${!containers[@]}"
do
    IFS=" " read -r name status <<< "${containers[i]}"
    # Color GREEN if service is active, else RED
    if [[ "${status}" == "Up" ]]
    then
      out+="${name}:,${GREEN}${status,,}${NC},"
    else
      out+="${name}:,${RED}${status,,}${NC},"
    fi
    # Insert \n every $DOCKER_COLUMNS column
    if [ $((($i+1) % $DOCKER_COLUMNS)) -eq 0 ]
    then
      out+="\n"
    fi
done
out+="\n"

echo -e "
${TITLE}Docker Status:$NC"
printf "$out" | column -ts $',' | sed -e 's/^/  /'
