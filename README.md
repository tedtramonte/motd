# Message of the Day
A message of the day script, modified and condensed from [yboetz's MOTD scripts](https://github.com/yboetz/motd).

## Requirements
### Optional
- `figlet`


## Setup
The best way to set up the MOTD is as part of a system's initial configuration through Ansible or an existing system image.

### RHEL/CentOS/Fedora
Place `motd.sh` in `/etc/profile.d/`
